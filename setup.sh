#!/bin/bash
apt-get -yqq update
apt-get -yqq upgrade
apt-get -y install libwebkit2gtk-4.0-dev build-essential libgtk-3-dev libappindicator-dev patchelf patchelf fuse appstream libstdc++-8-dev cmake clang libssl-dev zlib1g-dev libxxhash-dev git librust-pango-dev
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
nvm install --lts
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | bash -s -- -y
source $HOME/.cargo/env
cargo install cargo-chef
yarn global add yorkie
yarn global add lint-staged
cargo install tauri-cli --git https://github.com/tauri-apps/tauri
